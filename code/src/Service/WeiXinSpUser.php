<?php
namespace Service;
use EasyWeChat\Factory;

class WeiXinSpUser extends Base
{

    public function getApp()
    {
        $temp = getConfig('weixin', 'sp');
        $config = [
            'app_id' => $temp['app_id'],
            'secret' => $temp['app_secret'],
            'response_type' => 'array'
        ];
        $app = Factory::miniProgram($config);
        return $app;
    }

    public function getSession($code = '')
    {
        $app = $this->getApp();
        $result = $app->auth->session($code);
        return $result;
    }

    public function decryptPhoneInfo($sessionKey, $iv, $encryptedData)
    {
        $app = $this->getApp();
        $result = $app->encryptor->decryptData($sessionKey, $iv, $encryptedData);
        return $result;
    }

}