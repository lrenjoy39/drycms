<?php
namespace Service;

class File extends Base
{

    public function getDatabase()
    {
        return $this->get(DATABASE_FILE);
    }

    public function getCategoryCount()
    {
        $database = $this->getDatabase();
        return $database->getCategoryCount();
    }

    public function getListByCategory($page, $limit, $extension = [])
    {
        $database = $this->getDatabase();
        return $database->getListByCategory($page, $limit, $extension);
    }

    public function move($idIn, $categoryId)
    {
        $database = $this->getDatabase();
        $update = [
            'dry_category' => $categoryId
        ];
        return $database->updateMore($update, $idIn);
    }

    public function deletePlus($idIn)
    {
        if($idIn == ''){
            return false;
        }
        $list = $this->more($idIn);
        if(empty($list)){
            return false;
        }
        $idInOss = array2stringByKey($list, 'dry_oss_file');
        $serviceOssSimple = $this->get(SERVICE_OSSSIMPLE)->setConfig(THE_DEFAULT, true);
        foreach($list as $item){
            $serviceOssSimple->delete($item->dry_object);
        }
        $this->get(SERVICE_OSSFILE)->delete($idInOss);
        $this->delete($idIn);
    }

    public function rename($name, $id)
    {
        $database = $this->getDatabase();
        $update = [
            'dry_name' => $name
        ];
        return $database->update($update, $id);
    }

    public function getListByIds($idIn)
    {
        $database = $this->getDatabase();
        return $database->more($idIn);
    }

    /*
        $keys = [
            'dry_logo' => 1(表示只有1个文件)
            'dry_file' => 2(表示有多个文件)
        ];
    */
    public function getFileData($list, $keys = [])
    {
        $allFileId = [];
        foreach($list as $v){
            foreach($keys as $key => $n){
                if(!empty($v->{$key})){
                    $temp = explode(',', $v->{$key});
                    $allFileId = array_merge($allFileId, $temp);
                }
            }
        }
        $allFileId = array_unique($allFileId);
        $fileMap = [];
        if(!empty($allFileId)){
            $allFileId = implode(',', $allFileId);
            $tempList = $this->getListByIds($allFileId);
            $fileMap = array2mapObject($tempList, 'id');
        }
        foreach($list as $k => $v){
            foreach($keys as $key => $n){
                $list2 = [];
                $fileIds = explode(',', $v->{$key});
                foreach($fileIds as $fileId){
                    if(isset($fileMap[$fileId])){
                        $list2[] = $fileMap[$fileId];
                    }
                }
                $ext = $list2;
                if($n == 1){
                    if(empty($list2)){
                        $ext = object();
                    }
                    else{
                        $ext = $list2[0];
                    }
                }
                $v->{$key . '_ext'} = $ext;
                $list[$k] = $v;
            }
        }
        return $list;
    }

}