<?php
namespace Database;

class FileCategory extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_FILECATEGORY);
    }

    public function getList()
    {
        $sql = $this->getSql();
        $sql->table($this->getTable());
        $sql->where('dry_status', '=', 1);
        $sql->setOrder('dry_sort', 'asc');
        return $this->fetchAll($sql->get());
    }

}