<?php
namespace Cache;

class MessageCode extends Base
{

    private $prefix = 'mc';

    public function __construct()
    {
        $this->redis = $this->get(CORE_REDIS)->get('message_code');
    }

    public function getIpLastTimeKey($ip)
    {
        return sprintf('%s_ip_last_time_%s', $this->prefix, $ip);
    }

    public function getIpHourTotalKey($ip)
    {
        return sprintf('%s_ip_hour_total_%s_%s', $this->prefix, getHour(), $ip);
    }

    public function getPhoneLastTimeKey($phone)
    {
        return sprintf('%s_phone_last_time_%s', $this->prefix, $phone);
    }

    public function getPhoneHourTotalKey($phone)
    {
        return sprintf('%s_phone_hour_total_%s_%s', $this->prefix, getHour(), $phone);
    }

    public function getHourTotalKey()
    {
        return sprintf('%s_hour_total_%s', $this->prefix, getHour());
    }

    public function getDayTotalKey()
    {
        return sprintf('%s_day_total_%s', $this->prefix, getDateInt());
    }

    public function getTryTotalKey($phone)
    {
        return sprintf('%s_try_total_%s_%s', $this->prefix, getDateInt(), $phone);
    }

    public function getCodeKey($phone)
    {
        return sprintf('%s_code_%s', $this->prefix, $phone);
    }

    public function getIpLastTime($ip)
    {
        $key = $this->getIpLastTimeKey($ip);
        return (int)$this->redis->get($key);
    }

    public function getIpHourTotal($ip)
    {
        $key = $this->getIpHourTotalKey($ip);
        return (int)$this->redis->get($key);
    }

    public function getPhoneLastTime($phone)
    {
        $key = $this->getPhoneLastTimeKey($phone);
        return (int)$this->redis->get($key);
    }

    public function getPhoneHourTotal($phone)
    {
        $key = $this->getPhoneHourTotalKey($phone);
        return (int)$this->redis->get($key);
    }

    public function getHourTotal()
    {
        $key = $this->getHourTotalKey();
        return (int)$this->redis->get($key);
    }

    public function getDayTotal()
    {
        $key = $this->getDayTotalKey();
        return (int)$this->redis->get($key);
    }

    public function addTryTotal($phone)
    {
        $key = $this->getTryTotalKey($phone);
        return $this->redis->incr($key);
    }

    public function getTryTotal($phone)
    {
        $key = $this->getTryTotalKey($phone);
        return (int)$this->redis->get($key);
    }

    public function getCode($phone)
    {
        $key = $this->getCodeKey($phone);
        return $this->redis->get($key);
    }

    public function send($ip, $phone, $code)
    {
        $ipLastTimeKey = $this->getIpLastTimeKey($ip);
        $ipHourTotalKey = $this->getIpHourTotalKey($ip);
        $phoneLastTimeKey = $this->getPhoneLastTimeKey($phone);
        $phoneHourTotalKey = $this->getPhoneHourTotalKey($phone);
        $hourTotalKey = $this->getHourTotalKey();
        $dayTotalKey = $this->getDayTotalKey();
        $codeKey = $this->getCodeKey($phone);
        //
        $this->redis->setEx($ipLastTimeKey, 120, getTimestamp());
        $this->redis->incr($ipHourTotalKey);
        $this->redis->setEx($phoneLastTimeKey, 120, getTimestamp());
        $this->redis->incr($phoneHourTotalKey);
        $this->redis->incr($hourTotalKey);
        $this->redis->incr($dayTotalKey);
        $this->redis->setEx($codeKey, 900, $code);
        //
        $this->setExpire($ipHourTotalKey, 7200);
        $this->setExpire($phoneHourTotalKey, 7200);
        $this->setExpire($hourTotalKey, 7200);
        $this->setExpire($dayTotalKey, 86400 * 2);
    }

}