<?php
namespace Cache;

class Base
{

    use \CommonTrait\Base;

    public $redis = null;

    public function getRedis()
    {
        return $this->redis;
    }

    public function setExpire($key, $time)
    {
        if($this->redis->ttl($key) == -1){
            $this->redis->expire($key, $time);
        }
    }

    public function setData($name, $time, $data)
    {
        $key = $this->getKey($name);
        return $this->redis->setEx($key, $time, $data);
    }

    public function getData($name)
    {
        $key = $this->getKey($name);
        return $this->redis->get($key);
    }

    public function removeData($name)
    {
        $key = $this->getKey($name);
        return $this->redis->del($key);
    }

}