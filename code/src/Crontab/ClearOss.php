<?php
namespace Crontab;

class ClearOss extends Base
{

    /*
        /usr/bin/php -d display_error /app/cli.php local ClearOss.run
    */
    public function run()
    {
        $serviceOssSimple = $this->get(SERVICE_OSSSIMPLE)->setConfig(THE_DEFAULT, true);
        $nextMarker = '';
        while(1){
            $options = [
                'max-keys' => 100,
                'prefix' => 'upload/',
                'delimiter' => '',
                'marker' => $nextMarker
            ];
            $result = $serviceOssSimple->listObjects($options);
            $nextMarker = $result->getNextMarker();
            $listObject = $result->getObjectList();
            foreach($listObject as $object){
                $this->clear($serviceOssSimple, $object->getKey());
            }
            if($result->getIsTruncated() !== "true"){
                break;
            }
        }
    }

    public function clear($serviceOssSimple, $key = 'upload/image/2020/01/14/145724-495675.jpg')
    {
        $serviceFile = $this->get(SERVICE_FILE);
        $file = $serviceFile->getOneByField('dry_object', $key);
        if(!$file){
            $serviceOssSimple->delete($key);
            echo '已删除:' . $key . PHP_EOL;
        }
    }

}