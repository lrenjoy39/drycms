<?php
namespace Database;

class Choice extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_CHOICE);
    }

    public function getListByKey($key)
    {
        $table = $this->getTable();
        $sql = $this->getSql();
        $sql->field("{$table}.*");
        $sql->table($table);
        $sql->leftJoin($table, 'parent', 'id', '=', $table, 'dry_parent');
        $sql->where('parent.dry_key', '=', $key);
        $sql->setOrder("{$table}.dry_data_sort", 'asc');
        $list = $this->fetchAll($sql->get());
        return $list;
    }

}