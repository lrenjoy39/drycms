String.prototype.replaceAll = function(theFrom, to)
{
    var reg = new RegExp(theFrom, "g");
    return this.replace(reg, to);
};

function $$(id)
{
    return document.getElementById(id);
}

function contain(string, element)
{
    var theList = string.split(",");
    for(var i = 0;i < theList.length;i++){
        if(theList[i] == element){
            return true;
        }
    }
    return false;
}

function time()
{
    var d = new Date();
    return d.getTime();
}

function getDateTimeData(i = 0)
{
    function add0(m)
    {
        if(m < 10){
            return "0" + m;
        }
        return m;
    }

    var now = new Date();
    if(i){
        now = new Date(i * 1000);
    }
    var result = {
        "Y": now.getFullYear(),
        "m": add0(now.getMonth() + 1),
        "d": add0(now.getDate()),
        "H": add0(now.getHours()),
        "i": add0(now.getMinutes()),
        "s": add0(now.getSeconds())
    };
    result.standard = result.Y + "-" + result.m + "-" + result.d + " " + result.H + ":" + result.i + ":" + result.s;
    return result;
}

/*
    [min, max]
*/
function mtRand(min, max)
{
    var result = Math.random() * (max - min + 1) + min;
    return parseInt(result);
}

function HTMLEncode(html)
{
    var div = document.createElement("div");
    if(div.textContent != null){
        div.textContent = html;
    }
    else{
        div.innerText = html;
    }
    return div.innerHTML;
}

function HTMLDecode(text)
{
    var div = document.createElement("div");
    div.innerHTML = text;
    return (div.innerText || div.textContent);
}

function toFormData(data)
{
    var fd = new FormData();
    for(var key in data){
        fd.append(key, data[key]);
    }
    return fd;
}

function sendGetRequest(url, successCallback, failCallback)
{
    function innerSuccess(response)
    {
        if(response.ok == true && response.status == 200){
            response.json().then(function(json){
                successCallback(json);
            });
        }
    }

    function innerFail(response)
    {
        if(typeof(failCallback) == "function"){
            failCallback(response);
        }
        else{
            console.log(response);
        }
    }

    fetch(url).then(innerSuccess).catch(innerFail);
}

function sendPostRequest(url, data, successCallback, failCallback)
{
    function innerSuccess(response)
    {
        if(response.ok == true && response.status == 200){
            response.json().then(function(json){
                successCallback(json);
            });
        }
    }

    function innerFail(response)
    {
        if(typeof(failCallback) == "function"){
            failCallback(response);
        }
        else{
            console.log(response);
        }
    }

    fetch(url, {method: "post", body: data}).then(innerSuccess).catch(innerFail);
}

function checkPermission(attr, value, flag)
{
    var selector = "[@attr='@value']".replace('@attr', attr).replace('@value', value);
    $(selector).each(function(){
        var status = $(this).prop('checked');
        if(flag == 1){
            $(this).prop('checked', true);
        }
        else{
            $(this).prop('checked', !status);
        }
    });
    layui.form.render("checkbox");
}

function formatJSON(string)
{
    if(string == ""){
        return "";
    }
    var result = JSON.parse(string);
    return JSON.stringify(result, null, 4);
}

function getIdsStringForDelete(data)
{
    var list = [];
    for(var i=0;i<data.length;i++){
        list.push(data[i].id);
    }
    return list.join(",");
}