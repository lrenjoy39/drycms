<?php
function array2string($array = [])
{
    return implode(',', $array);
}

function array2stringByKey($list, $key = 'id')
{
    $map = [];
    foreach($list as $v){
        $temp = $v->{$key};
        $map[$temp] = $temp;
    }
    $list = array_values($map);
    return implode(',', $list);
}

function array2arrayByKey($list, $key = 'id')
{
    $map = [];
    foreach($list as $v){
        $temp = $v->{$key};
        $map[$temp] = $temp;
    }
    $list = array_values($map);
    return $list;
}

function array2mapObject($list, $key = 'id')
{
    $map = [];
    foreach($list as $v){
        $map[$v->{$key}] = $v;
    }
    return $map;
}

function formArray2string($data = [])
{
    foreach($data as $k => $v){
        if(is_array($v)){
            $data[$k] = implode(',', $v);
        }
    }
    return $data;
}