<?php
namespace Application\Admin\Controller;

class Home extends Base
{

    public function init()
    {
        $this->setDir('home');
    }

    public function indexAction()
    {
        $data = [
            'user' => $this->get(SERVICE_USER)->getUserFromToken('admin', $this->getToken()),
            'group' => $this->get(SERVICE_GROUP)->one($this->getGroup())
        ];
        $this->setFile($this->getTemplateIndex());
        $this->setData($data);
        $this->show();
    }

    public function loginAction()
    {
        $this->setFile($this->getTemplateLogin());
        $data = [
            'csrf' => csrfEncrypt('adminLogin')
        ];
        $this->setData($data);
        $this->show();
    }

    /*
        非法操作的页面
    */
    public function illegalAction()
    {
        $this->setTemplate('illegal', 'illegal');
        $this->setFile($this->getTemplate('illegal'));
        $this->show();
    }

    public function selectUserAction()
    {
        $token = $this->getToken();
        if(empty($token)){
            $this->jumpIllegal();
        }
        $serviceUser = $this->get(SERVICE_USER);
        $result = $serviceUser->validToken('admin', $token);
        if(!$result){
            $this->jumpIllegal();
        }
        $userId = $result['id'];
        $userIdIn = $this->get(SERVICE_DRY)->getSubUserList($userId, true);
        $userIdIn = implode(',', $userIdIn);
        $list = $this->get(SERVICE_USER)->more($userIdIn);
        /*禁用的用户踢掉 start*/
        foreach($list as $k => $v){
            if($v->dry_status == 0){
                unset($list[$k]);
            }
        }
        /*禁用的用户踢掉 end*/
        $this->setTemplate('selectUser', 'selectUser');
        $this->setFile($this->getTemplate('selectUser'));
        $data = [
            'list' => $list
        ];
        $this->setData($data);
        $this->show();
    }

    public function selectGroupAction()
    {
        $token = $this->getToken();
        if(empty($token)){
            $this->jumpIllegal();
        }
        $serviceUser = $this->get(SERVICE_USER);
        $result = $serviceUser->validToken('admin', $token);
        if(!$result){
            $this->jumpIllegal();
        }
        $userId = $result['id'];
        $user = $serviceUser->one($userId);
        /*选择的用户是否被禁用*/
        if($user->dry_status == 0){
            $this->jumpIllegal();
        }
        $list = $this->get(SERVICE_GROUP)->more($user->dry_group);
        $this->setTemplate('selectGroup', 'selectGroup');
        $this->setFile($this->getTemplate('selectGroup'));
        $data = [
            'list' => $list
        ];
        $this->setData($data);
        $this->show();
    }

}