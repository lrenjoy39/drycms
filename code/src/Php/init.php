<?php
date_default_timezone_set('Asia/Shanghai');
define('ENV', $argv[1]);
define('SRC_PATH', PROJECT_PATH . '/src');
define('PUBLIC_PATH', PROJECT_PATH . '/public');
define('CACHE_PATH', PROJECT_PATH . '/cache');
define('CONFIG_PATH', PROJECT_PATH . '/src/Config');
define('APPLICATION_PATH', PROJECT_PATH . '/src/Application');
define('CONSTANT_PATH', PROJECT_PATH . '/src/Constant');
define('FUNCTION_PATH', PROJECT_PATH . '/src/Function');
require_once(CONSTANT_PATH . '/system.php');
require_once(CONSTANT_PATH . '/service.php');
require_once(CONSTANT_PATH . '/data.php');
require_once(PROJECT_PATH . '/vendor/autoload.php');
/**/
$container = new Core\Container();
$container->load(CONFIG_PATH . '/service.yaml');
/**/
$applicationConfig = parse_ini_file(CONFIG_PATH . '/application.ini', true);
$container->setInstance('applicationConfig', $applicationConfig);
define('ADMIN_MODULE', $applicationConfig['admin']['module']);
define('ADMIN_LANGUAGE', $applicationConfig['admin']['language']);
/**/
$container->setInstance('language', getLanguage());
$container->setInstance('message', getMessage());
/**/
$loader = new Twig_Loader_Filesystem();
$loader->addPath(SRC_PATH . '/View');
foreach($applicationConfig['module']['list'] as $module){
    $twigPath = APPLICATION_PATH . "/{$module}/View";
    if(file_exists($twigPath)){
        $loader->addPath($twigPath, $module);
    }
}
$twig = new Twig_Environment($loader, ['debug' => true, 'auto_reload' => true, 'cache' => false]);
$twig->setCache(CACHE_PATH);
if(defined('CORE_TWIG')){
    $twig->addExtension($container->get(CORE_TWIG));
}
$container->setInstance('twig', $twig);
/**/
$domain = getConfig('oss', THE_DEFAULT)['domain'];
$content = "var OSS_DOMAIN = '{$domain}';";
file_put_contents(PUBLIC_PATH . '/js/oss_config.js', $content);