<?php
function getTimestamp()
{
    return time();
}

function getDateTime()
{
    $time = getTimestamp();
    return [
        'date' => date(THE_DATE_FORMAT, $time),
        'date_time' => date(THE_DATE_TIME_FORMAT, $time),
        'time' => date(THE_TIME_FORMAT, $time),
        'timestamp' => $time
    ];
}

function getDateTimeStd($i)
{
    return date(THE_DATE_TIME_FORMAT, $i);
}

function getHour()
{
    return date('H', getTimestamp());
}

function getDateInt()
{
    return date('Ymd', getTimestamp());
}

function getDateTimeInt()
{
    return date('YmdHis', getTimestamp());
}

function getDateHourInt()
{
    return date('YmdH', getTimestamp());
}

function getDateList($startDate, $endDate, $keyIsDate = false)
{
    $daySeconds = 86400;
    $startTimestamp = strtotime($startDate);
    $endTimestamp = strtotime($endDate);
    $span = intval(($endTimestamp - $startTimestamp) / $daySeconds) + 1;
    $list = [];
    for($i = 0;$i < $span;$i++){
        $temp = $startTimestamp + ($i * $daySeconds);
        $temp = date('Y-m-d', $temp);
        if($keyIsDate){
            $list[$temp] = $temp;
        }
        else{
            $list[] = $temp;
        }
    }
    return $list;
}

function getDateByOffset($date = '', $i = 0)
{
    if($date == ''){
        $date = date('Y-m-d', getTimestamp());
    }
    $daySeconds = 86400;
    $temp = strtotime($date) + ($i * $daySeconds);
    return date('Y-m-d', $temp);
}

/*当前时间与指定的时间相差的秒数*/
function getTimeSpan($time = '1970-01-02 03:04:05')
{
    $timeInt = strtotime($time);
    return getTimestamp() - $timeInt;
}

/*距离今天结束还有多少秒*/
function getTodayRemainSecond()
{
    $nowInt = getTimestamp();
    $end = date('Y-m-d 23:59:59', $nowInt);
    return strtotime($end) - $nowInt;
}

/*
    1557970560.5298
*/
function getMilliSecond()
{
    return microtime(true);
}

function getYestoday()
{
    return getDateByOffset('', -1);
}

function getTomorrow()
{
    return getDateByOffset('', 1);
}

function getWeekInfo()
{
    return [
        'start' => date('Y-m-d', strtotime('this week')),
        'end' => date('Y-m-d', strtotime('last day next week'))
    ];
}

function getMonthInfo()
{
    return [
        'start' => date('Y-m-d', strtotime('first day of now')),
        'end' => date('Y-m-d', strtotime('last day of now'))
    ];
}