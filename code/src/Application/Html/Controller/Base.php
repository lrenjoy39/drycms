<?php
namespace Application\Html\Controller;

class Base
{

    use \CommonTrait\Base;

    use \CommonTrait\Controller;

    use \CommonTrait\Template;

    public $request = null;

    public $response = null;

    public function __construct($request, $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->html();
        $this->setNameSpace('Html');
    }

}