<?php
namespace Service;

class Tool extends Base
{

    public function push2language()
    {
        $serviceModel = $this->get(SERVICE_MODEL);
        $modelList = $serviceModel->getList();
        $modelLanguage = $serviceModel->getModelByAlias('language');
        $serviceAutomate = $this->get(SERVICE_AUTOMATE)->setReference($modelLanguage);
        foreach($modelList as $model){
            $find = $serviceAutomate->getOneByField('dry_key', $model->dry_controller_name);
            if(empty($find)){
                $insert = [
                    'dry_group' => 'model',
                    'dry_key' => $model->dry_controller_name,
                    'dry_zh_cn' => $model->dry_name,
                    'dry_en_us' => $model->dry_name,
                    'dry_note' => 'tool'
                ];
                $insert = addDateTime($insert, THE_TIME);
                $serviceAutomate->add($insert);
            }
            else{
                $update = [
                    'dry_zh_cn' => $model->dry_name
                ];
                $serviceAutomate->update($update, $find->id);
            }
        }
    }

    public function language2file()
    {
        $serviceModel = $this->get(SERVICE_MODEL);
        $modelLanguage = $serviceModel->getModelByAlias('language');
        $serviceAutomate = $this->get(SERVICE_AUTOMATE)->setReference($modelLanguage);
        $list = $serviceAutomate->getLanguageForMakeFile();
        $data1 = [];
        $data2 = [];
        foreach($list as $item){
            $data1['default'][$item->dry_key] = $item->dry_zh_cn;
            $data2['default'][$item->dry_key] = $item->dry_en_us;
        }
        $file1 = CONFIG_PATH . '/common/language.zhCN.yaml';
        $file2 = CONFIG_PATH . '/common/language.enUS.yaml';
        array2yaml($file1, $data1);
        array2yaml($file2, $data2);
    }

    public function choice2file()
    {
        $serviceModel = $this->get(SERVICE_MODEL);
        $modelChoice = $serviceModel->getModelByAlias('choice');
        $serviceAutomate = $this->get(SERVICE_AUTOMATE)->setReference($modelChoice);
        $list = $serviceAutomate->getChoiceForMakeFile();
        $topList = [];
        $childrenList = [];
        foreach($list as $item){
            if($item->dry_grade == 1){
                $topList[] = $item;
            }
            else{
                $childrenList[$item->dry_parent][] = $item;
            }
        }
        $this->choice2JavaScriptFile($list, $topList, $childrenList);
        $this->choice2PhpFile($list, $topList, $childrenList);
    }

    private function choice2JavaScriptFile($list, $topList, $childrenList)
    {
        $js = [];
        foreach($topList as $item){
            if(!isset($childrenList[$item->id])){
                continue;
            }
            $js[] = "var {$item->dry_key}Data = [";
            foreach($childrenList[$item->id] as $k => $children){
                $temp = [
                    'key' => $children->dry_key,
                    'text' => $children->dry_value,
                    'class' => emptyReset($children->dry_class)
                ];
                $temp = '    ' . json_encode($temp, JSON_UNESCAPED_UNICODE);
                if($k == count($childrenList[$item->id]) - 1){
                    $js[] = $temp;
                }
                else{
                    $js[] = $temp . ',';
                }
            }
            $js[] = "];";
        }
        $js = implode(PHP_EOL, $js);
        file_put_contents('/app/public/js/dry-choice.js', $js);
    }

    private function choice2PhpFile($list, $topList, $childrenList)
    {

    }

    public function constant2file()
    {
        $serviceModel = $this->get(SERVICE_MODEL);
        $modelConstant = $serviceModel->getModelByAlias('constant');
        $serviceAutomate = $this->get(SERVICE_AUTOMATE)->setReference($modelConstant);
        $list = $serviceAutomate->getConstantForMakeFile();
        $this->constant2JavaScriptFile($list);
        $this->constant2PhpFile1($list);
        $this->constant2PhpFile2($list);
        $this->constant2PhpFile3($list);
    }

    private function constant2JavaScriptFile($list)
    {
        $js = [];
        foreach($list as $item){
            if($item->dry_is_int){
                $js[] = "var {$item->dry_constant_name} = {$item->dry_constant_value};";
            }
            else{
                $js[] = "var {$item->dry_constant_name} = '{$item->dry_constant_value}';";
            }
        }
        $js = implode(PHP_EOL, $js);
        file_put_contents('/app/public/js/dry-constant.js', $js);
    }

    private function constant2PhpFile1($list)
    {
        $code = [];
        $code[] = '<?php';
        foreach($list as $item){
            if($item->dry_is_int){
                $code[] = "define('{$item->dry_constant_name}', {$item->dry_constant_value});";
            }
            else{
                $code[] = "define('{$item->dry_constant_name}', '{$item->dry_constant_value}');";
            }
        }
        $code = implode(PHP_EOL, $code);
        file_put_contents(CONSTANT_PATH . '/data.php', $code);
    }

    private function constant2PhpFile2($list)
    {
        $code = [];
        $result = [];
        foreach($list as $item){
            $result[$item->dry_group][] = $item;
        }
        $code[] = "<?php";
        foreach($result as $group => $itemList){
            $name = 'check' . ucfirst($group);
            $code[] = "function {$name}(\$index)";
            $code[] = "{";
            $code[] = "    \$in = [";
            foreach($itemList as $k => $item){
                if(count($itemList) == $k + 1){
                    $code[] = "        {$item->dry_constant_name}";
                }
                else{
                    $code[] = "        {$item->dry_constant_name},";
                }
            }
            $code[] = "    ];";
            $code[] = "    return in_array(\$index, \$in);";
            $code[] = "}";
            $code[] = "";
        }
        $code = implode(PHP_EOL, $code);
        file_put_contents(FUNCTION_PATH . '/constant_check.php', $code);
    }

    private function constant2PhpFile3($list)
    {
        $code = [];
        $result = [];
        foreach($list as $item){
            $result[$item->dry_group][] = $item;
        }
        $code[] = "<?php";
        foreach($result as $group => $itemList){
            $name = 'get' . ucfirst($group) . 'Text';
            $code[] = "function {$name}(\$index)";
            $code[] = "{";
            $code[] = "    \$map = [";
            foreach($itemList as $k => $item){
                if(count($itemList) == $k + 1){
                    $code[] = "        {$item->dry_constant_name} => '{$item->dry_text}'";
                }
                else{
                    $code[] = "        {$item->dry_constant_name} => '{$item->dry_text}',";
                }
            }
            $code[] = "    ];";
            $code[] = "    if(isset(\$map[\$index])){";
            $code[] = "        return \$map[\$index];";
            $code[] = "    }";
            $code[] = "    else{";
            $code[] = "        return '未知';";
            $code[] = "    }";
            $code[] = "}";
            $code[] = "";
        }
        $code = implode(PHP_EOL, $code);
        file_put_contents(FUNCTION_PATH . '/constant_text.php', $code);
    }

}