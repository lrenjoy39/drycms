<?php
namespace Application\Admin\Controller;

class Base
{

    use \CommonTrait\Base;

    use \CommonTrait\Controller;

    use \CommonTrait\Template;

    public $request = null;

    public $response = null;

    public function __construct($request, $response)
    {
        $this->request = $request;
        $this->response = $response;
        $this->html();
        $this->setNameSpace('Admin');
        /*检查权限 start*/
        $bool = $this->get(SERVICE_PERMISSION)->check($request, $this->getRequestData(), $this->getGroup(), $this->getToken());
        if(!$bool){
            $this->jumpIllegal();
        }
        /*检查权限 end*/
    }

    public function jumpIllegal()
    {
        $this->jump('/Admin/Home/illegal');
    }

}