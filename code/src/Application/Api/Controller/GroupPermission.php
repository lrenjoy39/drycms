<?php
namespace Application\Api\Controller;

class GroupPermission extends Base
{

    private function getService()
    {
        return $this->get(SERVICE_GROUPPERMISSION);
    }

    public function newAction()
    {
        $data = $this->getRequestData();
        $id = csrfDecrypt($data['id']);
        if(!$id){
            $this->sendFail('CSRF_ERROR');
            return;
        }
        $service = $this->getService();
        $service->deleteByGroup($id);
        if(!isset($data['permission'])){
            $this->sendSuccess();
            return;
        }
        foreach($data['permission'] as $item){
            $insert = [
                'dry_group' => $id,
                'dry_route_name' => $item,
                'dry_note' => ''
            ];
            $insert = addDateTime($insert, THE_TIME);
            $service->add($insert);
        }
        $this->sendSuccess();
    }

}