<?php
namespace Database;

class User extends Base
{

    public function __construct()
    {
        $this->setModel(MODEL_USER);
    }

    public function getUserByUsernameOrPhone($username, $phone)
    {
        $sql = $this->getSql();
        $sql->field('*');
        $sql->table($this->getTable());
        $sql->where('dry_username', '=', $username);
        $sql->orWhere('dry_phone', '=', $phone);
        $sql->setPage(1);
        $sql->setPageSize(1);
        $sql->setLimit();
        return $this->fetch($sql->get());
    }

    public function weiXinSpAdd($phone, $openId, $userInfo)
    {
        $now = getDateTime();
        $insert = [
            'dry_group' => 3,
            'dry_username' => '',
            'dry_phone' => $phone,
            'dry_email' => '',
            'dry_password' => '',
            'dry_note' => '微信小程序自动注册',
            'dry_status' => 1,
            'dry_add_date' => $now['date'],
            'dry_add_time' => $now['date_time'],
            'dry_wei_xin_sp_open_id' => $openId,
            'dry_wei_xin_sp_user_info' => $userInfo
        ];
        return $this->insert($insert);
    }

}