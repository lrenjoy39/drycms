<?php
/*微信的钱单位是分*/
function money2weixin($amount)
{
    return (int)($amount * 100);
}

function makeOrderNo($userId, $type = 0)
{
    $now = date('YmdHis', getTimestamp());
    $rand = mt_rand(1000, 9999);
    $type = padding0($type, 2);
    $userId = padding0($userId, 10);
    return "{$now}{$type}{$userId}{$rand}";
}

/*订单是否过了支付时间*/
function isPayTimeout($orderTime)
{
    $end = strtotime($orderTime) + ORDER_TIME;
    return getTimestamp() > $end;
}

function formatPrice($i)
{
    return number_format($i, 2);
}

function getProductAgentMainProductSpec()
{
    return ['agent_t', 'agent_y', 'agent_j'];
}

function getProductAgentDiscount($mainProduct, $mainProductSpec)
{
    $map = [
        'drycms_agent_t' => 100,
        'drycms_agent_y' => 100,
        'drycms_agent_j' => 100
    ];
    $key = "{$mainProduct}_{$mainProductSpec}";
    if(isset($map[$key])){
        return $map[$key];
    }
    return 0;
}

function getProductAgentRate($mainProduct, $mainProductSpec)
{
    $map = [
        'drycms_agent_t' => 0.1,
        'drycms_agent_y' => 0.2,
        'drycms_agent_j' => 0.3
    ];
    $key = "{$mainProduct}_{$mainProductSpec}";
    if(isset($map[$key])){
        return $map[$key];
    }
    return 0;
}

function getSensitiveIdCard($idCard)
{
    $length = strlen($idCard);
    return substr($idCard, 0, 6) . str_repeat('*', $length - 10) . substr($idCard, $length - 4);
}

function getSensitiveBankCard($bankCard)
{
    $length = strlen($bankCard);
    return substr($bankCard, 0, 6) . str_repeat('*', $length - 10) . substr($bankCard, $length - 4);
}

function getMainProductSpec()
{
    return [
        'a' => YEAR_SECOND * 1 + DAY_SECOND * 1,
        'b' => YEAR_SECOND * 3 + DAY_SECOND * 3,
        'c' => YEAR_SECOND * 5 + DAY_SECOND * 5
    ];
}

/*drycms的vip版本*/
function getDrycmsVipVersionList()
{
    return [
        '1.0.1.zip',
        '1.0.0.zip'
    ];
}