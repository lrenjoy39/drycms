<?php
function startWith($string, $test)
{
    $result = strpos($string, $test);
    return ($result !== false && $result == 0);
}

function endWith($string, $test)
{
    $length = strlen($test);
    return substr($string, -$length) === $test;
}

function getSpace($grade, $number = 5)
{
    return str_repeat('-', ($grade - 1) * $number);
}

function compareValue($v1, $v2)
{
    if($v1 < $v2){
        return -1;
    }
    else if($v1 == $v2){
        return 0;
    }
    else{
        return 1;
    }
}

function object()
{
    return new \stdClass();
}

function rtrimPlus($string = '', $delete = '')
{
    return substr($string, 0, strlen($string) - strlen($delete));
}

function array2yaml($file, $data)
{
    $content = yaml_emit($data, YAML_UTF8_ENCODING, YAML_CRLN_BREAK);
    $content = str_replace('---', '', $content);
    $content = str_replace('...', '', $content);
    $content = preg_replace('/^(  +)/m', '$1$1', trim($content));
    file_put_contents($file, $content);
}

function emptyReset($data, $value = '')
{
    if(empty($data)){
        return $value;
    }
    return $data;
}

/*
    $list = [
        ['id'=> 1, 'name'=>'2'],
        ['id'=> 2, 'name'=>'1']
    ];
    $id = array_column($list, 'id');
    $name = array_column($list, 'name');
    array_multisort($id, SORT_ASC, $list);
    array_multisort($id, SORT_ASC, $name, SORT_ASC, $list);
*/
function orderByField($list, $field, $sort)
{
    $data = array_column($list, $field);
    array_multisort($data, $sort, $list);
    return $list;
}

function setAndNotEmpty($array, $key)
{
    if(isset($array[$key]) && !empty($array[$key])){
        return true;
    }
    else{
        return false;
    }
}

function notSetOrEmpty($array, $key)
{
    if(!isset($array[$key]) || empty($array[$key])){
        return true;
    }
    else{
        return false;
    }
}

function notSet($array, $key)
{
    if(!isset($array[$key])){
        return true;
    }
    else{
        return false;
    }
}

function tryGet($data, $key, $default = '')
{
    if(isset($data[$key])){
        return $data[$key];
    }
    return $default;
}

function isPhone($phone)
{
    preg_match_all('/^1[3-9]{1}\d{9}$/', $phone, $result);
    return isset($result[0][0]);
}

function isBankCard($bankCard)
{
    preg_match_all('/^\d{16,32}$/', $bankCard, $result);
    return isset($result[0][0]);
}

function isIdCard($idCard)
{
    $length = strlen($idCard);
    return in_array($length, [15, 18]);
}

function padding0($data, $length)
{
    $hasLength = strlen($data);
    $spanLength = $length - $hasLength;
    if($spanLength <= 0){
        return $data;
    }
    return str_repeat('0', $spanLength) . $data;
}

function getSensitivePhone($phone)
{
    return substr($phone, 0, 3) . '****' . substr($phone, 7, 11);
}

function getTotalPage($total, $pageSize = 10)
{
    return (int)ceil($total / $pageSize);
}

function json_encode_plus($data)
{
    return json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
}

function contain($string, $value)
{
    $in = explode(',', $string);
    return in_array($value, $in);
}

function sendGetByCurl($url)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $return = curl_exec($ch);
    curl_close($ch);
    return $return;
}

function makeUserCode($id)
{
    $id = bcadd($id, '10000000000');
    $list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    $count = count($list);
    $code = [];
    while($id){
        $mod = $id % $count;
        $id = (int)($id / $count);
        $code[] = $list[$mod];
    }
    return implode('', $code);
}

function parseUserCode($code)
{
    $id = 0;
    $list = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    $count = count($list);
    $listFlip = array_flip($list);
    $length = strlen($code);
    for($i = 0;$i < $length;$i++){
        $temp = bcmul($listFlip[$code[$i]], pow($count, $i));
        $id = bcadd($id, $temp);
    }
    $id = bcsub($id, '10000000000');
    return $id;
}

function saveObjectKey($data, $keys = [])
{
    $result = object();
    foreach($keys as $key){
        $result->{$key} = $data->{$key};
    }
    return $result;
}