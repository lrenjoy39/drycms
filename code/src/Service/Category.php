<?php
namespace Service;

class Category extends Base
{

    public function setGrade($serviceAutomate, $id)
    {
        $data = $serviceAutomate->one($id);
        $update = [];
        if($data->dry_parent == 0){
            $update['dry_grade'] = 1;
        }
        else{
            $rs = $serviceAutomate->one($data->dry_parent);
            $update['dry_grade'] = $rs->dry_grade + 1;
        }
        $serviceAutomate->update($update, $id);
        /**/
        $config = [
            'table' => $serviceAutomate->getTable()
        ];
        $sqls = getCategoryUpdateSortSql($config);
        $serviceAutomate->setDataSort($sqls);
    }

}