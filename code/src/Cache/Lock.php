<?php
namespace Cache;

class Lock extends Base
{

    private $prefix = 'lock';

    public function __construct()
    {
        $this->redis = $this->get(CORE_REDIS)->get('lock');
    }

    private function getKey($name)
    {
        return sprintf('%s_%s', $this->prefix, $name);
    }

    public function hasLock($name, $time)
    {
        $key = $this->getKey($name);
        $i = $this->redis->incr($key);
        $this->setExpire($key, $time);
        if($i > 1){
            return true;
        }
        return false;
    }

    public function removeLock($name)
    {
        $key = $this->getKey($name);
        return $this->redis->del($key);
    }

}